/*
	Данный пример кода иллюстрирует работу скрипта javascript для работы игры сапёр.
	
*/

$(document).ready(function(){
	var tds = $('td');
	for(var i = 0; i < tds.length; i++){
		tds.eq(i).attr("data-id", i);
	}
	var empty = 0;
	var unknown = 0;
	
	tds.click(function(){
		var id = $(this).attr("data-id");
		for(var i = 0; i < tds.length; i++){
			if(tds.eq(i).css("backgroundColor") == 'rgba(0, 0, 0, 0)'){
				unknown++;
			}
		}
		$.ajax({
			type : 'POST',
			url : 'script.php',
			data : 'id=' + $(this).attr("data-id") + '&empty=' + empty + '&unknown=' + unknown,
			dataType : 'html',
			cache : false,
			success : function(data){
				if(data == 'You win!'){
					alert('Вы выиграли');
					$.ajax({
						type : 'POST',
						url : 'break_session.php',
						cache: false,
						success : function(data){
							for(var i = 0; i < tds.length; i++){
								tds.eq(i).css("backgroundColor", "white");
								tds.eq(i).html("");
								$('#p_mines').hide();
								$('#chance').hide();
							}
						}
					});
					location.reload();
				} else if(data == 'Взрыв'){
					tds.eq(id).css("backgroundColor", "red");
					alert('Вы проиграли');
					$.ajax({
						type : 'POST',
						url : 'break_session.php',
						cache: false,
						success : function(data){
							for(var i = 0; i < tds.length; i++){
								tds.eq(i).css("backgroundColor", "white");
								tds.eq(i).html("");
								$('#p_mines').hide();
								$('#chance').hide();
							}
						}
					});
					location.reload();
				} else {
					data = data.split(',');
					tds.eq(id).css("backgroundColor", "green");
					tds.eq(id).html(data[1]);
					empty++;
					$('#p_mines').show();
					$('#mines').html(data[2]);
					$('#chance').show();
					$('#current_chance').html(data[3]);
					unknown = 0;
				}
			}
		});
	});
	
	var guess = $('.guess');
	guess.click(function(e){
		var old_bomb_label = $('#bomb_label').html();
		for(var i = 0; i < tds.length; i++){
			if(tds.eq(i).css("backgroundColor") == 'rgba(0, 0, 0, 0)'){
				unknown++;
			}
		}
		if(old_bomb_label == 0){
			alert('Вы исчерпали лимит меток!');
		} else {
			var id = $(this).parent().attr("data-id");
			var e = e || window.event;
			var target = e.target || e.srcElement;
			if(this == target){
				$.ajax({
					type : 'POST',
					url : 'bomb_label.php',
					data : 'id=' + id + '&unknown=' + unknown,
					dataType : 'html',
					cache : false,
					success : function(data){
						if(data == 'Вы не угадали местоположение бомбы'){
							var new_bomb_label = $('#bomb_label').html() - 1;
							$('#bomb_label').html(new_bomb_label);
							alert(data);
						} else if(data == 'Вы успешно нашли все бомбы! Поздравляем!') {
							alert(data);
							$.ajax({
								type : 'POST',
								url : 'break_session.php',
								cache: false,
								success : function(data){
									for(var i = 0; i < tds.length; i++){
										tds.eq(i).css("backgroundColor", "white");
										tds.eq(i).html("");
										$('#p_mines').hide();
										$('#chance').hide();
									}
								}
							});
							location.reload();
						} else {
							data = data.split(',');
							tds.eq(id).css("backgroundColor", "brown");
							$('#chance').show();
							$('#current_chance').html(data[1]);
							tds.eq(id).html("");
							var new_count_mines = $('#mines').html() - 1;
							$('#mines').html(new_count_mines);
							alert(data[0]);
						}
					}
				});
			}
		}
	});
});