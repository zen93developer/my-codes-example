/*
	Данный пример кода иллюстрирует работу скрипта для целей авторизации и аутентификации
*/

$(document).ready(function(){
	$('#button_enter_auth').click(function(){
		if($('#block_top_auth').css('display') == 'none'){
			$('#block_top_auth').fadeIn(200);
		} else {
			$('#block_top_auth').fadeOut(200);
		}
	});
	
	/* Показывать/скрыть пароль */
	$('#button_pass_show_hide').click(function(){
		if($('#auth_pass').attr("type") == "password"){
			$('#auth_pass').attr("type", "text");
			$('#button_pass_show_hide').text("Пароль виден");
		} else {
			$('#auth_pass').attr("type", "password");
			$('#button_pass_show_hide').text("Пароль скрыт");
		}
	});
	
	/* Проверять все ли данные введены при аутентификации */
	$('#button_auth').click(function(){
		var auth_login = $('#auth_login').val();
		var auth_pass = $('#auth_pass').val();
		var send_login = false;
		var send_pass = false;
		var auth_remember_me = false;
		if(auth_login == "" || auth_login.length > 30){
			$('#auth_login').css("border", "1px solid red");
			send_login = false;
		} else {
			$('#auth_login').css("border", "1px solid #DBDBDB");
			send_login = true;
		}
		if(auth_pass == "" || auth_pass.length > 30){
			$('#auth_pass').css("border", "1px solid red");
			send_pass = false;
		} else {
			$('#auth_pass').css("border", "1px solid #DBDBDB");
			send_pass = true;
		}
		if($('#remember_me').prop('checked')){
			auth_remember_me = true;
		} else {
			auth_remember_me = false;
		}
		if(send_login == true && send_pass == true){
			$.ajax({
				type : "POST",
				url : "include/auth.php",
				data : "login=" + auth_login + "&pass=" + auth_pass + "&remember_me=" + auth_remember_me,
				dataType : "html",
				cache : false,
				success : function(data){
					if(data == 'yes_auth'){
						location.reload();
					} else {
						$('#message_auth').slideDown(400);
					}
				}
			});
		}
	});
});