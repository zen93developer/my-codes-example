import Vue from 'vue';
import Vuex from 'vuex';
import {category} from "./category";
import {personal} from "./personal";
import {contacts} from "./contacts";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    category,
    personal,
    contacts
  }
});
