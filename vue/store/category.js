export const category = {
  state: {
    category: []
  },
  mutations: {
    setCategories(state, data){
      state.category = data;
    }
  },
  actions: {
    setCategories({state, commit}, data){
      commit('setCategories', data);
    }
  },
  getters: {

  }
};
