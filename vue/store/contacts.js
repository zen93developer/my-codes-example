export const contacts = {
  state: {
    address: '',
    phone: '',
    email: '',
    googleMapSrc: ''
  },
  mutations: {
    setAddress(state, data){
      state.address = data;
    },
    setPhone(state, data){
      state.phone = data;
    },
    setEmail(state, data){
      state.email = data;
    },
    setGoogleMapSrc(state, data){
      state.googleMapSrc = data;
    }
  },
  actions: {
    setAddress({state, commit}, data){
      commit('setAddress', data);
    },
    setPhone({state, commit}, data){
      commit('setPhone', data);
    },
    setEmail({state, commit}, data){
      commit('setEmail', data);
    },
    setGoogleMapSrc({state, commit}, data){
      commit('setGoogleMapSrc', data);
    },
  },
  getters: {

  }
}
