export const personal = {
  state: {
    personal: [],
    category: []
  },
  mutations: {
    setPersonal(state, data){
      state.personal = data;
    },
    setPersonalCategory(state, data){
      state.category = data;
    }
  },
  actions: {
    setPersonal({state, commit}, data){
      commit('setPersonal', data);
    },
    setPersonalCategory({state, commit}, data){
      commit('setPersonalCategory', data);
    }
  },
  getters: {
    getRightFormatPersonal: state => {
      let temporary = {};

      if(state.category.data !== undefined && state.personal.data !== undefined)
      {
        for(let i = 0; i < state.category.data.length; i++){
          temporary[state.category.data[i].title] = [];
          for(let j = 0; j < state.personal.data.length; j++){
            if(state.personal.data[j].category === state.category.data[i].id)
            {
              temporary[state.category.data[i].title].push(state.personal.data[j]);
            }
          }
        }
      }

      return temporary;
    }
  }
};
