<?php
/**
 * Created by PhpStorm.
 * User: Evgeny
 * Date: 31.12.2017
 * Time: 18:56
 */
 
    /*
	
		Здесь приведён пример кода, демонстрирующий работу маршрутизатора WEB_приложения, построенного по архитектуре MVC с использованием чистого PHP
	
	*/

    class Route
    {
        static function start()
        {
            // отбрасываем возможные элементы GET-запроса
            $url = explode('?', $_SERVER['REQUEST_URI']);
            $get = $url[1];

            // получаем данные из адресной строки и разбиваем их в массив
            $parts_url = explode('/', $url[0]);

            // определяем имя контроллера и действия
            $controller = (!empty($parts_url[1])) ? $parts_url[1] : 'main';
            $action = (!empty($parts_url[2])) ? $parts_url[2] : 'index';

            // подключаем нужную модель
            $model_file = 'model_'.$controller.'.php';
            $model_path = 'app/models/'.$model_file;
            if(file_exists($model_path)) include $model_path;

            // подключаем нужный контроллера
            $controller_file = 'controller_'.$controller.'.php';
            $controller_path = 'app/controllers/'.$controller_file;
            if(file_exists($controller_path)) include $controller_path;
            //else self::ErrorPage404();

            // создаём контроллер
            $controller_name = 'Controller_'.$controller;
            $controller = new $controller_name;

            // выполняем нужное действие
            $action_name = 'action_'.$action;
            if(method_exists($controller, $action_name)) $controller->$action_name();
            //else self::ErrorPage404();
        }
    }

?>