const express = require('express');
const database = require('./database.js');
const bodyParser = require('body-parser');
const fileUpload = require('express-fileupload');
const cors = require('cors');

var slideModel = require('./models/slide.js');
var contactModel = require('./models/contact.js');
var personalModel = require('./models/personal.js');
var aboutModel = require('./models/about.js');
var adminModel = require('./models/admin.js');

const unirest = require('unirest');
const cheerio = require('cheerio');

database()
  .then(response => {

  const app = express();

  app.use(express.static('dist'));
  app.use(bodyParser.urlencoded({extended:true})); // parse application/x-www-form-urlencoded
  app.use(bodyParser.json()); // parse application/json
  app.use(fileUpload()); // для загрузки файлов на сервер
  app.use(cors({origin:true, credentials:true})); // стобы не было ошибки No Access-Control-Allow-Origin

  app.get('/getContacts', (req, res) => {
    contactModel.find({}).then(contacts => {
      let contactsValue = [];
      for(let i = 0; i < contacts.length; i++){
        contactsValue.push(contacts[i].value);
      }
      res.send(contactsValue);
    });
  });

  app.get('/getPersonal', (req, res) => {
    personalModel.find({}).then(personal => {
      let personalArray = [];
      personal.forEach((man, index, array) => {
        personalArray.push(man);
      });
      res.send(personalArray);
    });
  });

  app.get('/getEvents', (req, res) => {
    // парсер событий ДМС
    let events = [];
    unirest.get('http://www.jinr.ru/about/events-plan/').end(response => {
      const $ = cheerio.load(response.body); // сохраняем полученную разметку в специальный объект
      let blockEvent = $('.b-plan__item > a');
      blockEvent.find($('.b-plan-item__where')).each(function(i, elem){
        if($(this).text().indexOf('ДМС') !== -1){
          let title = $(this).parent().find($('.b-plan-item__title')).text();
          let href = $(this).parent().attr('href');
          let period = $(this).parent().find($('.b-plan-item__period')).text();
          events.push({
            title,
            href,
            period
          });
        }
      });
      res.send(events);
    });
  });

  app.get('/getSlides', (req, res) => {
    slideModel.find({})
      .then(slides => {
        res.send(slides);
      })
      .catch(error => {console.log(error)});
  });

  app.get('/getInfoAbout', (req, res) => {
    aboutModel.find({'title':'Описание'})
      .then(text => res.send(text))
      .catch(error => {console.log(error)});
  });

  app.get('*', (req, res) => {
    res.sendFile(__dirname + '/index.html');
  });

  app.post('/updateContacts', (req, res) => {
    contactModel.findOneAndUpdate({'title':'Телефон'}, {'value':req.body.phone}, {upset:false}, (err, doc) => {});
    contactModel.findOneAndUpdate({'title':'Email'}, {'value':req.body.email}, {upset:false}, (err, doc) => {});
    contactModel.findOneAndUpdate({'title':'mapSrc'}, {'value':req.body.mapsrc}, {upset:false}, (err, doc) => {});
    contactModel.findOneAndUpdate({'title':'Адрес'}, {'value':req.body.address}, {upset:false}, (err, doc) => {});
  });

  app.post('/updateSlide', (req, res) => {
    if(req.body.photo === '') {
      slideModel.findOneAndUpdate({'_id':req.body.id}, {'title':req.body.title}, {upset:false}, (err, doc) => {});
    }
    else {
      // загружаем файл
      if (!req.files)
        return res.status(400).send('No files were uploaded.');

      let sampleFile = req.files.photo;
      sampleFile.mv(__dirname + '/dist/slide_images/'+ sampleFile.name, function(err) {console.log(err)});

      slideModel.findOneAndUpdate({'_id':req.body.id}, {'title':req.body.title, 'img':sampleFile.name}, {upset:false}, (err, doc) => {});

      res.send('ok');
    }
  });

  app.post('/createNewPersonal', (req, res) => {
    if (!req.files)
      return res.status(400).send('No files were uploaded.');

    let sampleFile = req.files.photo;
    sampleFile.mv(__dirname + '/dist/photo_personal/' + sampleFile.name, function (err) {
      console.log(err)
    });
    personalModel.create({
      position: req.body.position,
      fio: req.body.fio,
      email: req.body.email,
      phone: req.body.phone,
      photo: sampleFile.name,
      duties: req.body.duties
    });
  });

  app.post("/updatePersonal", (req, res) => {
    if (!req.files)
      return res.status(400).send('No files were uploaded.');

    let sampleFile = req.files.photo;
    sampleFile.mv(__dirname + '/dist/photo_personal/' + sampleFile.name, function (err) {console.log(err)});

    personalModel.findOneAndUpdate({'_id':req.body.id}, {
      'position':req.body.position, 'fio':req.body.fio, 'phone':req.body.phone,
      'email':req.body.email, 'duties':req.body.duties, 'photo':sampleFile.name
    }, {upset:false}, (err, doc) => {});
  });

  app.post("/updateTextAbout", (req, res) => {
    aboutModel.findOneAndUpdate({"title":"Описание"}, {"value":req.body.text}, {upset:false}, (err, doc) => {});
    res.send("Ok!");
  });

  app.post('/checkAdminPermission', (req, res) => {
    adminModel.findOne({"login":req.body.login})
      .then(user => {
        res.send("All is good!");
      })
      .catch(err => {console.log(err)});
  });

  app.post('/deletePersonal', (req, res) => {
    personalModel.findOneAndDelete({'_id':req.body.id}, (err, result) => {});
    res.send('Ok!');
  });

  app.listen(process.env.PORT || 3031, function(){console.log("Server started!")});

})
  .catch(error => {
  console.log("Database error! App won't start!");
});
