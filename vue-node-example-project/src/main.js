import Vue from 'vue'
import App from './App.vue'
import VueBootstrap from 'bootstrap-vue';
import VueRouter from 'vue-router';
import Routers from './router';
import VueResouce from 'vue-resource';
import Store from './store';
import VueCookie from 'vue-cookie';

Vue.use(VueBootstrap);
Vue.use(VueRouter);
Vue.use(VueResouce);
Vue.use(VueCookie);

Vue.http.options.root = 'http://localhost:4545/';

import 'bootstrap/dist/css/bootstrap.css';
import 'bootstrap-vue/dist/bootstrap-vue.css';

new Vue({
  el: '#app',
  store: Store,
  render: h => h(App),
  router: Routers,
})
