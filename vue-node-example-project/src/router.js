import VueRouter from 'vue-router';
import StartPage from './components/start-page';
import MainPage from './components/main-page';
import ProductPage from './components/product-page';
import BasketPage from './components/basket-page';
import EditPage from './components/edit-page';

export default new VueRouter({
  routes: [
    {
      path: '',
      component: StartPage
    },
    {
      path: '/main',
      component: MainPage
    },
    {
      path: '/product',
      component: ProductPage
    },
    {
      path: '/product/:id',
      component: ProductPage
    },
    {
      path: '/basket',
      component: BasketPage
    },
    {
      path: '/edit',
      component: EditPage
    },
  ],
  mode: 'history'
});

