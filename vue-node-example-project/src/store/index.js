import Vue from "vue";
import Vuex from 'vuex';

Vue.use(Vuex);

import {user} from './user';
import {users} from './users';
import {basket} from './basket';

export default new Vuex.Store({
  modules: {
    user,
    users,
    basket
  }
});
