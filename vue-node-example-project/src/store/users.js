export const users = {
  state: {
    users: []
  },
  mutations: {
    setUsers(state, data){
      state.users = data;
    }
  },
  actions: {
    setUsers({commit}, data){
      commit('setUsers', data);
    }
  },
  getters: {
    // функция нужна дял того, чтобы в списке доступных для себя пользователей небыло активного пользователя
    getUsersForDialog(){
      return function (ownerId) {
        let usersArr = [];
        for(let i = 0; i < users.state.users.data.length; i++) {
          if(users.state.users.data[i].id !== ownerId) usersArr.push(users.state.users.data[i]);
        }
        return usersArr;
      };
    }
  }
};
