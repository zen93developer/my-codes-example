export const basket = {
  state: {
    goods: [],
  },
  mutations: {
    addGoodToBasket(state, data){
      state.goods.push(data);
    },
    fillBasket(state, data){
      state.goods = data;
    },
  },
  actions: {
    addGoodToBasket({commit}, data){
      commit('addGoodToBasket', data);
    },
    fillBasket({commit}, data){
      commit('fillBasket', data);
    },
  },
  getters: {

  }
}
