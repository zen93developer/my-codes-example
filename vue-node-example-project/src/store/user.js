export const user = {
  state: {
    id: null,
    login: '',
    password: '',
    photo: '',
  },
  mutations: {
    setId(state, data){
      state.id = data;
    },
    setLogin(state, data){
      state.login = data;
    },
    setPassword(state, data){
      state.password = data;
    },
    setPhoto(state, data){
      state.photo = data;
    }
  },
  actions: {
    setId({commit}, data){
      commit('setId', data);
    },
    setLogin({commit}, data){
      commit('setLogin', data);
    },
    setPassword({commit}, data){
      commit('setPassword', data);
    },
    setPhoto({commit}, data){
      commit('setPhoto', data);
    }
  },
  getters: {

  }
}
