const express = require('express');
const bodyParser = require('body-parser');
const sqlite3 = require('sqlite3').verbose();
const fileUpload = require('express-fileupload');

let database = new sqlite3.Database('database.db');

const app = express();

app.use(express.static('dist'));
app.use(bodyParser.urlencoded({extended:true})); // parse application/x-www-form-urlencoded
app.use(bodyParser.json()); // parse application/json
app.use(fileUpload()); // для загрузки файлов на сервер

app.get('/getAllUsers', (req, res) => {
  database.all("SELECT * FROM users", (error, rows) => {
    if(rows.length > 0){
      res.send(JSON.stringify(rows));
    } else res.send("error");
  });
});

app.get('/getAllProducts', (req, res) => {
  database.all(`SELECT * FROM products`, (error, rows) => {
    if(rows.length > 0){
      res.send(JSON.stringify(rows));
    } else res.send("error");
  });
});

app.get('/getBasketCount', (req, res) => {
  database.all(`SELECT * FROM basket WHERE userId = ${req.query.id}`, (error, rows) => {
    if(rows.length > 0){

      // формируем строку для перечисления id товаров
      let goodsId = rows.map(function(item, index, arr){
        return item.goodId;
      });
      goodsId = goodsId.join(',');

      // теперь нам надо получить не просто ссылки на товары а сами товары
      database.all(`SELECT * FROM products WHERE id IN (${goodsId})`, (error, rows) => {
        if(rows.length > 0)res.send(rows);
        else res.send('error');
      });
    } else res.send('error');
  });
});

app.get('/getProductsById', (req, res) => {
  database.all(`SELECT * FROM products WHERE ownerId = ${+req.query.id}`, (error, rows) => {
    if(rows.length > 0){
      res.send(JSON.stringify(rows));
    } else res.send("error");
  });
});

app.get('*', (req, res) => {
  res.sendFile(__dirname + '/index.html');
});

app.post('/checkUserEnter', (req, res) => {
  database.all(`SELECT * FROM users WHERE login = "${req.body.login}"`, (error, rows) => {
    if(rows.length > 0){
      if(rows[0].password === req.body.password){
        res.send(rows[0]);
      }
      else res.send("error");
    } else res.send("error");
  });
});

app.post('/registerUser', (req, res) => {

  // проверяем есть ли такой пользователь уже в базе данных
  database.all(`SELECT * FROM users WHERE login = "${req.body.login}"`, (error, rows) => {
    if(rows.length > 0){
      res.send('error');
    }
    else {

      // если нет, то заносим его в базу данных
      database.run(`INSERT INTO users VALUES(NULL, "${req.body.login}", "${req.body.password}")`, (error, rows) => {

        // после этого надо выбрать его из базы данных, что бы корректно занести в состояние приложения
        database.all(`SELECT * FROM users WHERE login = "${req.body.login}"`, (error, rows) => {
          res.send(rows[0]); // потому что всегда возвращается массив объектов
        });
      });
    }
  });
});

app.post('/startDialog', (req, res) => {
  database.all(`SELECT * FROM dialogs WHERE user1 = ${req.body.to} OR user1 = ${req.body.from}`, (error, rows) => {
    if(rows.length == 0){
      database.run(`INSERT INTO dialogs VALUES(NULL, ${req.body.from}, ${req.body.to})`, (error, rows) => {
        res.send("Dialog created");
      });
    } else {
      database.all(`SELECT * FROM messages WHERE dialog = ${rows[0].id}`, (error, rows) => {
        if(rows.length > 0) res.send(rows);
      });
    }
  });
});

app.post('/addMessage', (req, res) => {
  if(req.body.text != ''){

    // сначала заносим сообщение вбазу данных
    database.run(`INSERT INTO messages VALUES(NULL, "${req.body.text}", ${req.body.dialogId}, ${req.body.userId}, "${req.body.userLogin}")`, (error, rows) => {

      // а потом снова загружаем все сообщения, чтобы сразу отобразить новое сообщение
      database.all(`SELECT * FROM messages WHERE dialog = ${req.body.dialogId}`, (error, rows) => {
        if(rows.length > 0) res.send(rows);
      });
    });
  }
});

app.post('/addGoodToBasket', (req, res) => {
  database.run(`INSERT INTO basket VALUES(NULL, ${req.body.userId}, ${req.body.goodId})`, (error, rows) => {
    res.send('ok');
  });
});

app.post('/deleteGood', (req, res) => {
  database.run(`DELETE FROM products WHERE id = ${req.body.id}`, (error, rows) => {
    res.send('ok');
  });
});

app.post('/addGood', (req, res) => {
  if(req.body.title != '' && req.body.description != ''){
    database.run(`INSERT INTO products VALUES(NULL, "${req.body.title}", "${req.body.description}", ${req.body.ownerId})`);
    res.send('ok');
  }
});

app.post('/editUser', (req, res) => {
  if (!req.files) return res.send('error');

  let photo = req.files.photo;
  photo.mv(__dirname + '/dist/photo/' + photo.name, function(err) {
    if (err) return res.send('error');
  });

  database.run(`UPDATE users SET login = '${req.body.login}', 
                password = '${req.body.password}', photoName = '${photo.name}'
                WHERE id = ${req.body.id}`, (error, rows) => {});

  res.send('ok');
});

app.listen(4545, () => {console.log("Server started successfully!")});
