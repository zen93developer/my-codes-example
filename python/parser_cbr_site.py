# -*- coding: utf-8 -*-

# данный притмер кода иллюстрирует работу парсера на языке программирования Python

from bs4 import BeautifulSoup
import urllib.request


class ParserCBRSite():
    currency_and_values = {}

    def __init__(self, date):
        # date must be format - 19.04.2018
        self.url = 'http://www.cbr.ru/currency_base/daily.aspx?date_req=' + date

    def get_html(self):
        response = urllib.request.urlopen(self.url)
        return response

    def parse(self):
        soup = BeautifulSoup(self.get_html(), 'html.parser')
        main_table_with_data = soup.find('table', class_='data')
        for row in main_table_with_data.findAll('tr')[1::]:
            currency = row.findAll('td')[3].text
            value = row.findAll('td')[4].text
            # необходимо реформантнуть полученную строку в формат, который можно фортматнуть в double
            value = value.replace(',', '.')
            self.currency_and_values[currency] = value